const elementUtils = require("../util/elementUtils")

class LoginPage{
//page elements
get signinlink(){
    return $('a')
}
get emailtestfiled(){
    return $("(//*[text()='Email address']//following::input[1])[1]")
}
get createbutton(){
    return $('#SubmitCreate')
}
get radiobutton(){
    return $("//*[text()='Title']//following::input[1]")
}
get firstname(){
    return $("(//*[text()='First name ']//following::input[1])[1]")
}
get lastname(){
    return $("(//*[text()='Last name ']//following::input[1])[1]")
}
get email(){
    return $("(//*[text()='Email ']//following::input[1])[1]")
}
get password(){
    return $("(//*[text()='Password ']//following::input[1])[1]")
}
get dob_day(){
    return $('select#days')
}
get dob_month(){
    return $('select#months')
}
get dob_year(){
    return $('select#years')
}
get address1(){
    return $("//*[text()='Address ']//following::input[1]")
}
get city(){
    return $("//*[text()='City ']//following::input[1]")
}
get state(){
    return $("select#id_state")
}
get zipcode(){
    return $("//*[text()='Zip/Postal Code ']//following::input[1]")
}
get mobilenumber(){
    return $("input#phone_mobile")
}
get register(){
    return $('button#submitAccount')
}

//page operations
getPageTitle(){
    return elementUtils.getPageTitle()
}

doSignin(email,fName,lName,password,address,city,zipcode,mobilenumber){
    elementUtils.clickElement(this.signinlink)
    ElementUtil.setValueToElement(this.emailtestfiled, email)
    ElementUtil.clickElement(this.createbutton)
    ElementUtil.clickElement(this.radiobutton)
    ElementUtil.setValueToElement(this.firstname, fName)
    ElementUtil.setValueToElement(this.lastname, lName)
    ElementUtil.setValueToElement(this.email, email)
    ElementUtil.setValueToElement(this.password, password)
    ElementUtil.setValueToElement(this.address1, address)
    ElementUtil.setValueToElement(this.city, city)
    ElementUtil.setValueToElement(this.zipcode, zipcode)
    ElementUtil.setValueToElement(this.mobilenumber, mobilenumber)
    ElementUtil.clickElement(this.register)
}




}

module.exports = new LoginPage()
