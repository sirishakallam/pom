class ElementUtil{

clickElement(element){
    element.waitForDispalyed()
    element.click()

}
setValueToElement(element, value){
    element.waitForDispalyed()
    element.setValue(value)
    
}
getTextForElement(element){
    element.waitForDispalyed()
    return element.getText()
}
getPageTitle(pageTile){
    browser.waitUntil(function(){
        return(browser.getTitle()== pageTitle)
    },10000,'title is not displaayed after the given time'
    )
    return browser.getTitle()
}
isDisplayed(element){
    element.waitForDispalyed()
    return element.isDisplayed()
}


}

module.exports= new ElementUtil()