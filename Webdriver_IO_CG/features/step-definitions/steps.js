import { Given, When, Then,And,Examples } from 'cucumber';
const config = require("../config")
import LoginPage from '../pageobjects/sign.page';
import SecurePage from '../pageobjects/secure.page';
import signPage from '../pageobjects/sign.page';

const pages = {
    Signin: signPage
}


Given(/^launch the site$/, (page) => {
    pages[page].open()
});

When(/^click on the sign in button and enter the required details$/, () => {
    signinPage.doSignin(config.email,config.fName,config.lName,config.email,config.password,config.address1,config.city,config.zipcode,config.mobilenumber)
});

